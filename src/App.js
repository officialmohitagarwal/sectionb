import * as React from 'react';
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import data from './data.json';
import './App.css';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';

const mainDataContent = [
  {
    id: 1,
    name: 'Documents',
    children: [
      {
        id: 2,
        name: 'Document1.jpg',
      },
      {
        id: 3,
        name: 'Document2.jpg',
      },
      {
        id: 4,
        name: 'Document3.jpg',
      },
    ],
  },
  {
    id: 5,
    name: 'Desktop',
    children: [
      {
        id: 6,
        name: 'Screenshot1.jpg',
      },
      {
        id: 7,
        name: 'videopal.mp4',
      },
    ],
  },
  {
    id: 8,
    name: 'Downloads',
    children: [
      {
        id: 9,
        name: 'Drivers',
        children: [
          { id: 10, name: 'PrinterDriver.dmg' },
          { id: 11, name: 'CameraDriver.dmg' },
        ],
      },
      {
        id: 12,
        name: 'Applications',
        children: [
          { id: 13, name: 'Webstorm.dmg' },
          { id: 14, name: 'Pycharm.dmg' },
          { id: 15, name: 'Filezilla.dmg' },
          { id: 16, name: 'Mattermost.dmg' },
        ],
      },
      {
        id: 17,
        name: 'chromedriver.dmg',
      },
    ],
  },
];

export default function FileSystemNavigator() {
  const [selectedNode, setSelectedNode] = React.useState({});
  const [selectedId, setSelectedId] = React.useState(0);
  const [mainData, setMainData] = React.useState(mainDataContent);
  const [searchText, setSearchText] = React.useState('');

  // React.useEffect(() => {
  //   console.log('selectedNode', selectedNode);
  //   console.log('selectedRoo', selectedRoot);
  // }, [selectedNode, selectedRoot]);
  const addNode = () => {
    if (selectedNode) {
      if (selectedNode.children) {
        selectedNode.children.push({
          id: Math.floor(Math.random() * 1000),
          name: searchText,
        });
        setMainData([
          ...mainData.map((ele) =>
            ele.id !== selectedId ? ele : selectedNode
          ),
        ]);
      } else {
        selectedNode.children = [
          {
            id: Math.floor(Math.random() * 1000),
            name: searchText,
          },
        ];
        setMainData([
          ...mainData.map((ele) =>
            ele.id !== selectedId ? ele : selectedNode
          ),
        ]);
      }
    }
  };

  const updateNode = () => {
    if (selectedNode) {
      if (selectedNode.children) {
        selectedNode.name = searchText;
        setMainData([
          ...mainData.map((ele) =>
            ele.id !== selectedId ? ele : selectedNode
          ),
        ]);
      } else {
        selectedNode.name = searchText;
        setMainData([
          ...mainData.map((ele) =>
            ele.id !== selectedId ? ele : selectedNode
          ),
        ]);
      }
    }
  };

  const deleteNode = () => {
    if (selectedNode.children) {
      selectedNode.name = null;
      selectedNode.id = null;
      selectedNode.children = null;
      setMainData([
        ...mainData.map((ele) => (ele.id !== selectedId ? ele : selectedNode)),
      ]);
      setSearchText('');
    } else {
      selectedNode.name = null;
      selectedNode.id = null;
      setMainData([
        ...mainData.map((ele) => (ele.id !== selectedId ? ele : selectedNode)),
      ]);
      setSearchText('');
    }
  };

  const handleChange = (event, nodeId) => {
    findEle(mainData, nodeId);
  };

  const findEle = (arr, nodeId) => {
    const parentArr = arr.filter((ele) => ele.id === Number(nodeId));
    if (parentArr.length === 0) {
      arr.forEach((ele) => {
        if (Array.isArray(ele.children)) {
          findEle(ele.children, nodeId);
        }
      });
    } else {
      setSearchText(parentArr[0].name);
      setSelectedNode(parentArr[0]);
      setSelectedId(Number(nodeId));
    }
  };

  const renderTree = (nodes) => {
    if (Array.isArray(nodes)) {
      return nodes.map((ele) => (
        <TreeItem key={ele.id} nodeId={`${ele.id}`} label={ele.name}>
          {Array.isArray(ele.children)
            ? ele.children.map((node) => renderTree(node))
            : null}
        </TreeItem>
      ));
    } else {
      return (
        <TreeItem key={nodes.id} nodeId={`${nodes.id}`} label={nodes.name}>
          {Array.isArray(nodes.children)
            ? nodes.children.map((node) => renderTree(node))
            : null}
        </TreeItem>
      );
    }
  };

  return (
    <div className='container'>
      <div style={{ width: 500, padding: '2rem' }}>
        <TextField
          id='name'
          variant='outlined'
          value={searchText}
          onChange={(e) => setSearchText(e.target.value)}
          fullWidth
        />
        <Grid container spacing={2} sx={{ mt: 1 }}>
          <Grid item xs={4}>
            <Button
              variant='contained'
              fullWidth
              sx={{ py: 1.2 }}
              onClick={updateNode}
            >
              Update Item
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button
              variant='contained'
              fullWidth
              sx={{ py: 1.2 }}
              onClick={addNode}
            >
              Add Item
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button
              variant='contained'
              fullWidth
              sx={{ py: 1.2 }}
              onClick={deleteNode}
            >
              Remove Item
            </Button>
          </Grid>
        </Grid>
      </div>
      <TreeView
        onNodeSelect={handleChange}
        aria-label='file system navigator'
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}
        sx={{
          flexGrow: 1,
          maxWidth: 400,
          overflowY: 'auto',
        }}
      >
        {/* {Object.keys(data).map((ele, i) => (
          <TreeItem nodeId={(i + 1).toString()} key={i} label={ele}>
            {getData(ele, i)}
          </TreeItem>
        ))} */}
        {renderTree(mainData)}
      </TreeView>
      <div style={{ height: '2rem' }}></div>
    </div>
  );
}
